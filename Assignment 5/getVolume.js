class Shape {
  constructor(radius, height){
    this.height = height;
    this.radius = radius;
    this.result = 0;
  }
}

class Cylinder extends Shape{

  constructor(radius,height){
    super(radius, height);
  }

  getVolume(){
    this.result = Math.PI * (this.radius * this.radius) * this.height;
    console.log(`Cylinder Volume with radius as ${this.radius} and height ${this.height} is ${this.result}`)
  }

}

class Sphere extends Shape{

  constructor(radius){
    super(radius);
  }

  getVolume(){
    this.result = (4 / 3) * ( Math.PI * (this.radius * this.radius * this.radius));
    console.log(`Sphere Volume with radius as ${this.radius} is ${this.result}`)
  }

}

class Cone extends Shape{

  constructor(radius,height){
    super(radius, height);
  }

  getVolume(){
    this.result = ((Math.PI * (this.radius * this.radius)) * this.height) / 3;
    console.log(`Cone Volume with radius as ${this.radius} and height ${this.height} is ${this.result}`)
  }

}

let sphere1 = new Sphere(2);
sphere1.getVolume();

let cylinder1 = new Cylinder(2, 3);
cylinder1.getVolume();

let cone1 = new Cone(2, 3);
cone1.getVolume();