class Rectangle{
  constructor(length, width){
    this.length = length;
    this.width = width;
    this.result = 0;
  }

  area(){
    this.result = this.length * this.width;
    console.log(`Area of length${this.length} and width ${this.width} is ${this.result}`)
  }
}

class Square extends Rectangle{
  constructor(length, width){
    super(length,width);
  }
}

let rectangle1 = new Rectangle(2,3);
rectangle1.area();

let square1 = new Square(2,2);
square1.area()