const sleepFunction = () =>{
  return 'Sleep'
}

const sleepPromise = new Promise((resolve, reject) =>{
  setTimeout(() =>{
    resolve('Sleep in progress...')
  }, 3000)
})

const sleepConsumer = () =>{
  sleepPromise.then(result =>{
    console.log(result);
  })
}

sleepConsumer();