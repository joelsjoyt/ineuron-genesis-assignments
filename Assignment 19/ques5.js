const delay = () =>{
  return new Promise(resolve =>{
    setTimeout(() => {
      resolve('Delay resolved')
    },3000)
  })
}

delay().then(result => console.log(result));