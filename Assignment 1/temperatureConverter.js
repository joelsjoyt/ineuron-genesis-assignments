"use strict"

function tempChecker(value, unit){
  let result = 0;
  if(unit){
    //Fahrenheit to Celsius
    result = ((value - 32) * 5) /9
  }
  else{
    //Celsius to Fahrenheit
    result = (value * (9/5)) + 32
  }

  return result;
}


//Celsius = 0 Fahrenheit = 1

//Celsius
console.log(`60 Celsius is ${tempChecker(60, 0)} Fahrenheit`);
console.log(`7.2 Celsius is ${tempChecker(7.2222222222222222, 0)} Fahrenheit`);

//Fahrenheit
console.log(`45 Fahrenheit is ${tempChecker(45,1)} Celsius`);
console.log(`140 Fahrenheit is ${tempChecker(140,1)} Celsius`);