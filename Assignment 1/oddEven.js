"use strict"

let num = 1;

while(num <= 50){
  if(num % 2 === 0){
    console.log(`${num} is even`);
  }
  else{
    console.log(`${num} is odd`);
  }
  num++
}