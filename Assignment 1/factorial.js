"use strict"

function findFactorial(value){
  let result = 1;
  for(let i = 1 ; i <= value; i++){
    result = result * i;
  }
  return result;
}

let num = 5;
console.log(`Factorial of ${num} is ${findFactorial(num)}`)

