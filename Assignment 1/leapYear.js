"use strict"

let checkLeapYear = (year) =>{
  if(year % 4 === 0)
  return true;
  else
  return false;
}

let year = 2016;

checkLeapYear(year)
?
console.log(`${year} is a leap year`)
:
console.log(`${year} is not a leap year`)



