console.log("\n-------- ** --------\n")

console.log("\nPrime number generator apart from 2 and 3 \n");

let i = 1;

while(i <= 10){
  let temp1 = 0;
  let temp2 = 0;
  temp1 = 6 * i + 1;
  temp2 = 6 * i - 1;
  console.log(`Prime number ${temp2}`);
  console.log(`Prime number ${temp1}`);
  i++
}

console.log("\n-------- ** --------\n")

console.log("\nPrime number Checker\n")

let number = 2

while( number <= 50){
  let checker = 2
  let flag = true
  while(checker < parseInt(number / 2)){
    if (number %  checker != 0 )
    {
      flag =  true;
      checker++;
    }
    else{
      flag = false;
      break;
    }
  }
  flag 
  ? 
  console.log(`${number} is prime`)
  :
  console.log(`${number} is not prime`)
  number++
}

// let number = 2

// while( number <= 10){
//   let checker = 2
//   let flag = true
//   while(checker <= parseInt(number / 2)){
//     if(number %  checker != 0) {
//       checker++
//     } 
//     else{
//       flag = false
//       break;
//     }
//   }
//   if(flag){
//     console.log(`${number} is prime`)
//   }
//   else{
//     console.log(`${number} is not prime`)
//   } 
//   number++
// }