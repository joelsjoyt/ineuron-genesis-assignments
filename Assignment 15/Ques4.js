let calculateDotCount = (value) =>{
  let prevCount = 0;
  let currentCount = 0;
  let cumilativecount = [];
  for(let i = 1; i <= value; i++){
    currentCount = prevCount + i
    prevCount = currentCount
    cumilativecount.push(currentCount);
  }
  
  return { "count" : currentCount, "sumOfCounts" : cumilativecount.reduce((acc, curr) => acc += curr) } ;
}

let {count, sumOfCounts} = calculateDotCount(6);
console.log(`Count ${count}, Sum of counts ${sumOfCounts}`);