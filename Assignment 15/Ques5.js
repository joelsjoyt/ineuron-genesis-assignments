let changeEnough = (arr, value) =>{
  let total = [];
  total.push(arr[0] * 0.25);
  total.push(arr[1] * 0.10);
  total.push(arr[2] * 0.05);
  total.push(arr[3] * 0.01);

  if(total.reduce((acc, curr) => acc += curr,0) >= value){
    return true;
  }else{
    return false
  }
    
}

console.log(changeEnough([2, 100, 0, 0], 14.11)); 
console.log(changeEnough([0, 0, 20, 5], 0.75)); 
console.log(changeEnough([30, 40, 20, 5], 12.55)); 