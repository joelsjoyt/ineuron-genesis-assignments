let nextEdge = (side_1, side_2) =>{
  return Math.abs((side_1 + side_2) - 1);
}

console.log(nextEdge(8,10));
console.log(nextEdge(5,7));
console.log(nextEdge(9,2));