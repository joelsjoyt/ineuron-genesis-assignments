let hospitals = [
  {
  "id": 1,
  "name":"Hospital A",
  "location":"Delhi",
  "noOfBeds":450,
  "availability":"Yes"
  },
  {
  "id": 2,
  "name":"Hospital B",
  
  "location":"Pune",
  "noOfBeds":150,
  "availability":"No"
  },
  {
  "id": 3,
  "name":"Hospital C",
  "location":"Pune",
  "noOfBeds":350,
  "availability":"Yes"
  }]

console.log(hospitals.filter((hospital) => hospital.noOfBeds > 200));
console.log("---------------****------------");
console.log(hospitals.filter((hospital) => hospital.availability === "Yes"));
console.log("---------------****------------");
console.log(hospitals.filter((hospital) => hospital.location === 'Pune'));