let factorial = (value) =>{
  result = 1
  for( let i = 1; i <= value; i++ ){
    result *= i
  }
  return result
}



function specialNumber(value){
  let result = 0;
  let number = value;
  while( number >= 1){
    remainder = number % 10;
    result += factorial(remainder);
    number /= 10;
  }
  
  result === value ?
  console.log(`${value} is a special number`)
  :
  console.log(`${value} is not a special number`)
}

specialNumber(145);
specialNumber(1209);

