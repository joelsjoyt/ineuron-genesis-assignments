require("@nomicfoundation/hardhat-toolbox");
require("hardhat-gas-reporter");
require("dotenv").config()

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.17",
   gasReporter:{
    enabled: true,
    currency: "INR",
    noColors: true,
    outputFile: "gasReport.txt",
    coinmarketcap: process.env.CMC_API_KEY,
  },
  settings: {
      optimizer: {
        enabled: true,
        runs: process.env.HARDHAT_OPTIMIZER_RUNS,
      }
    }    
};
