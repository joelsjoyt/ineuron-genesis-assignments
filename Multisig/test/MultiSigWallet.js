const { ethers } = require("hardhat");
const { expect } = require("chai");

describe("MultiSigWallet Contract Test", function(){
  let multiSigWallet;
  let accounts;


  const amount = ethers.utils.parseEther("0.3");

  before(async () =>{
    accounts = await ethers.getSigners();
    const contract = await ethers.getContractFactory("MultiSigWallet");
    multiSigWallet = await contract.deploy([accounts[0].address, accounts[1].address], 1);
    await multiSigWallet.deployed();
  });

  it("Check owners for MultiSigWallet", async () =>{
    const [owner1, owner2] = await multiSigWallet.getOwners();
    expect(owner1).to.equal(accounts[0].address);
    expect(owner2).to.equal(accounts[1].address);
  });

  it("Unauthorised signers do not have permission to submit transaction", async ()=>{
    const wallet = multiSigWallet.connect(accounts[2]);
    await expect(wallet.submitTransaction(accounts[3].address, 10, 0x1234)).to.be.reverted;
  });

  it("Unauthorised signers do not have permission to confirm transaction", async ()=>{
    const wallet = multiSigWallet.connect(accounts[2]);
    await expect(wallet.confirmTransaction(0)).to.be.reverted;
  });

  it("Unauthorised signers do not have permission to revoke confirmation", async ()=>{
    const wallet = multiSigWallet.connect(accounts[2]);
    await expect(wallet.revokeConfirmation(0)).to.be.reverted;
  });
  
  it("Unauthorised signers do not have permission to execute transaction", async ()=>{
    const wallet = multiSigWallet.connect(accounts[2]);
    await expect(wallet.executeTransaction(0)).to.be.reverted;
  });

  it("Submit transcation", async () =>{
    const wallet = multiSigWallet.connect(accounts[0]);
    const beforeSubmission = await multiSigWallet.getTransactionCount();
    await wallet.submitTransaction(accounts[3].address, 10, 0x1234);
    expect(await multiSigWallet.getTransactionCount()).to.equal(beforeSubmission.add(1));
  });

  it("Confirm Transaction", async () =>{
    const wallet = multiSigWallet.connect(accounts[1]);
    const beforeConfirmation = await multiSigWallet.transactions(0);
    await wallet.confirmTransaction(0);
    const afterConfirmation = await multiSigWallet.transactions(0)
    expect(afterConfirmation.numConfirmations).to.equal(beforeConfirmation.numConfirmations.add(1));
  });

  it("Revoke Confirmation", async () =>{
    const wallet = multiSigWallet.connect(accounts[1]);
    const beforeConfirmation = await multiSigWallet.transactions(0);
    await wallet.revokeConfirmation(0);
    const afterConfirmation = await multiSigWallet.transactions(0)
    expect(afterConfirmation.numConfirmations).to.equal(beforeConfirmation.numConfirmations.sub(1));
  });

  it("Revoke cannot be done by the different owners", async () =>{
    const wallet = multiSigWallet.connect(accounts[0]);
    await wallet.confirmTransaction(0);
    const wallet2 = multiSigWallet.connect(accounts[1])
    await expect(wallet2.revokeConfirmation(0)).to.be.reverted;  
  });

  it("Need ethers in contract to execute transcation", async () =>{
    const wallet = multiSigWallet.connect(accounts[0]);
    await expect(wallet.executeTransaction(0)).to.be.reverted;
  });

  it("Need confirmations to execute transcation", async () =>{
    const wallet = multiSigWallet.connect(accounts[0]);
    await wallet.revokeConfirmation(0);
    await expect(wallet.executeTransaction(0)).to.be.reverted;
  });

  it("Deposit Ether in contract", async () =>{
    const wallet = multiSigWallet.connect(accounts[0]);
    const currentBalance = await ethers.provider.getBalance(multiSigWallet.address);
    await wallet.DepositETH({value:amount});
    expect(await ethers.provider.getBalance(multiSigWallet.address)).to.equal(currentBalance.add(amount) );
  });

  it("Deposit Ether via reveive fallback function", async () =>{
    const currentBalance = await ethers.provider.getBalance(multiSigWallet.address);
     await accounts[0].sendTransaction({
      to: multiSigWallet.address,
      value: amount
    });
    expect(await ethers.provider.getBalance(multiSigWallet.address)).to.equal(currentBalance.add(amount));
  });

  it("Execute Transaction", async () =>{
    const wallet = multiSigWallet.connect(accounts[0]);
    await wallet.confirmTransaction(0);
    await wallet.executeTransaction(0);
    const transaction = await multiSigWallet.transactions(0);
    expect(transaction.executed).to.be.true;    
  });

  it("Cannot revoke confirmation after execution", async () =>{
    const wallet = multiSigWallet.connect(accounts[0]);
    await expect(wallet.revokeConfirmation(0)).to.be.reverted; 
  });

  it("Cannot confirm transaction after execution", async () =>{
    const wallet = multiSigWallet.connect(accounts[0]);
    await expect(wallet.confirmTransaction(0)).to.be.reverted; 
  });

});