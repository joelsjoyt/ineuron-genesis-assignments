let array = [1,2,3,[4,[5,6]]];

const flatten = (arr) =>{
  let result = [];
  arr.forEach(element => {
    if(Array.isArray(element)){
        let temp = flatten(element)
        result.push(...temp);
    }else{
      result.push(element)
    }
  });

  return result
}

console.log(flatten(array));