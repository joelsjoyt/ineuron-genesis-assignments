var arr =[]
var evenSum = 0;
var oddSum = 0;
var evenArray = [];
var oddArray = [];


// //Save values
// let save = () => {
//    arr.push(Number(document.getElementById('li').value))
   
// }

//Save values
let save = () => {
   let input = document.getElementsByName('a1')
   for(let i = 0; i<input.length; i++){
      arr.push(Number(input[i].value))
   }
   
}

//GCD function
let gcd = (value1, value2) =>{
   while(value2){
      let x = value2;
      value2 = value1 % value2;
      value1 = x;
   }

   return value1
}

//Find even or odd
let findEvenOrOdd = () =>{
   arr.forEach(element => {
      if(element % 2 === 0){
         evenArray.push(element);
      }
      else{
         oddArray.push(element);
      }
   });
}

//Show Values
let show = (id = 'val1', val = arr) =>{
  document.getElementById(id).innerText = val;
  
}

//Sum array
let calculate = () =>{
 evenArray.forEach(element => {
   evenSum += element
 });
 oddArray.forEach(element => {
   oddSum += element
 });
 show('soe', evenSum);
 show('soo', oddSum);
}

//Difference of even and odd sum
let difference = () =>{
   show('dos',Math.abs(evenSum - oddSum) )
}

//Number of even elements
let noOfEven = () =>{
   show('noe', evenArray.length);
}

//Number of oddelements
let noOfOdd = () =>{
   show('noo', oddArray.length);
}

//Calculate average
let average = () =>{
   show('avg', (evenSum + oddSum)/arr.length)
}


let calculateGCD = () =>{
   let x = gcd(evenSum, oddSum)
   show('gcd', x)
}

