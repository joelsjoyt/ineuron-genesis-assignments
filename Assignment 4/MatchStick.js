function matchHouse(step){
  baseSticks = 6;
  upgradeSticks = 5;
  completedHouse = 6 + (5 *(step-1))
  return completedHouse
}

console.log(`\n Step 1 has ${matchHouse(1)} matches`); // Returns matches needed for spep 1 matchouse

console.log("\n----------------*----------------\n")

console.log(`\n Step 4 has ${matchHouse(4)} matches`); // Returns matches needed for spep 4 matchouse

console.log("\n----------------*----------------\n")

console.log(`\n Step 87 has ${matchHouse(87)} matches`); // Returns matches needed for spep 87 matchouse

console.log("\n----------------*----------------\n")

// Shows matches for n number of steps
// for( let i = 0; i <=number; i ++){
//   console.log(`Step ${i} has ${matchHouse(i)} matches`)
// }