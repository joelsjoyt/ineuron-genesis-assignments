let arr1 = ["a", "b", "c", "d"];
let arr2 = ["e", "f", "g", "h", "a", "i", "j"]

let itr1 = arr1[Symbol.iterator]();
let itr2 = arr2[Symbol.iterator]();

let res1 = itr1.next()
let res2 = itr2.next()

while(!res2.done){
  while(!res1.done){
    if(res1.value === res2.value){
      console.log(res1.value);
    }
    res1 = itr1.next();
  }
  res2 = itr2.next();
  itr1 = arr1[Symbol.iterator]();
  res1 = itr1.next()
}

