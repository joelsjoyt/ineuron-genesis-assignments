let array = [1,2,3,4,5,6];

//Method 1 using iterators and generators

function* arrayIterator(arr){
  for(let i =0; i<arr.length; i++){
    yield arr[i];
  }
} 

let iterMethod1 = arrayIterator(array);
console.log(iterMethod1.next().value);

console.log("---------------********---------------");

//Method 2 Using symbols

let iterMethod2 = array[Symbol.iterator]();
console.log(iterMethod2.next().value);