function* arrayIterator(arr){
  for(let i =0; i<arr.length; i++){
    yield arr[i];
  }
} 

let arr = "ABCDE";

let itr = arrayIterator(arr);
console.log(itr.next());
console.log(itr.next());
console.log(itr.next());