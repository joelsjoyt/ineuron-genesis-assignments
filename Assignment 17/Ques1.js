let array = ["John", "Rohn", "Danny", "James"];

function* arrayIterator(arr){
  for(let i =0; i<arr.length; i++){
    yield arr[i];
  }
} 

let itr = arrayIterator(array)
let result =  itr.next();
while(!result.done){
  console.log(result);
  result = itr.next();
}


