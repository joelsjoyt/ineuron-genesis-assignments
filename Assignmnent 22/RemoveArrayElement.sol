// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

contract ArrayIndexDelete{

    uint8[] arr;

    function setData(uint8 data) public{
        arr.push(data);
    }
    
    function getData() public view returns(uint8[] memory){
        return arr;
    }

    function shiftAndDelete(uint8 index) public{
        
        uint8 temp; 

        for(uint i = index; i<arr.length - 1; i++){
            
                temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
            
        }

        arr.pop();
    }

}