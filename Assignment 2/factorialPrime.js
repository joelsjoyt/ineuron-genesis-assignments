function checkPrime(value){
  //Checks if a number is prime or not
  let flag = false;
  if( value === 0 || value === 1){
    flag = false;
  }
  else if( value === 2 || value === 3){
    flag = true;
  }
  
  for( let i = 2; i<=parseInt(value/2); i++){
    if( value % i === 0){
      flag = false;
      break;
    }
    else flag = true;
  }

  return flag;
}

function factorial(value){
  //Finds factorial of a number
  let result = 1;
  let final = {}
  for(let i = 1; i<=value; i++){
    result = result * i;
  }
  final[value] = result;
  return final;
}

function factorialInRange(range1, range2){
  let a = [];
  for( let i = range1 + 1; i<range2; i++){
    if(checkPrime(i)){
      temp = factorial(i);
      a.push(temp)
    }
  }
  return a
}

result =  factorialInRange(1,100);
console.log(result);