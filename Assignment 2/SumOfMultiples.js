// Finds sum of multiples of 3 and 5

let x = 1000;
let sum = 0;
for( let i = 0; i<=x; i++){
  if(i % 3 === 0 && i % 5 === 0){
    sum += i;
  }
}

console.log(sum);