function findMarks(mark){
  switch(true){
    case (mark > 90 && mark <=100):{
      console.log(`${mark} - S Grade`);
      break; 
    }
    case (mark > 80 && mark <=90):{
      console.log(`${mark} - A Grade`);
      break; 
    }
    case (mark > 70 && mark <=80):{
      console.log(`${mark} - B Grade`);
      break; 
   }
    case (mark > 60 && mark <=70):{
      console.log(`${mark} - C Grade`);
      break; 
    }
    case (mark > 50 && mark <=60):{
      console.log(`${mark} - D Grade`);
      break; 
    }
    case (mark > 40 && mark <=50):{
      console.log(`${mark} - E Grade`);
      break; 
    }
    case (mark >= 0 && mark <=40):{
      console.log(`${mark} - Student has failed`);
      break; 
    }
    default:
      console.log(`${mark} is an invalid mark`)
    break;   
  }
}

findMarks(12);
findMarks("abc");
findMarks(67);