/*
Ques 1
Higher Order Functions and Call back functions are different
Callback functions are arguments/function passed to Higher Order Function 
to perform some logic
Higher Order Functions accepts a function as an argument

Ques 2
Filter is a Higher Order Function because it accepts a function as its argument
*/

// Ques 3

//Callback function
function callbk(value){
  return value * value;
}

//Higher order function
function hof(x, fun){
  console.log(fun(x))
}

hof(3, callbk);

/*Ques 4 
A. Hello John
   Hello Tina
   Hello Kale
   Hello max

B. useFunction() is a Higher Order Function here
*/