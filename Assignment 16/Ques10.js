let XO = (value) =>{
  let countO =  value.match(/o/gi).length
  let countX =  value.match(/x/gi).length
  if(countO === countX){
    return true
  }else{
    return false
  }
}

console.log(XO("xxoo"));
console.log(XO("xooxx"));
console.log(XO("ooxXm"));