let checkPrime = (value) =>{
  const check = /^1?$|^(11+?)\1+$/;
  value = Array(value + 1).join(1)
  // console.log(value)
  return !check.test(value)
}

console.log(checkPrime(3));
console.log(checkPrime(7));
console.log(checkPrime(10));
console.log(checkPrime(8));