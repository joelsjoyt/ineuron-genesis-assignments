let isValidHexCode = (value) =>{
  let check = /^(#([A-F]|\d){6})\b/g;
  return check.test(value);
}

console.log(isValidHexCode("#CD5C5C"));
console.log(isValidHexCode("#EAECEE"));
console.log(isValidHexCode("#CD5C&C"));
