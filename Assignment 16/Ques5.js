let removeZeroes = (val) =>{
  val = val.replace(/(^0+)/, '')
  val = val.replace(/\.?(0+$)/, '')
  return val
}

console.log(removeZeroes("004020"));
console.log(removeZeroes("00402.0"));
console.log(removeZeroes("004.020"));