let checkpin = (val) =>{
  let check = /(\b\d{4}\b)|(\b\d{6}\b)/g;
  return check.test(val)
}

console.log(checkpin("123"));
console.log(checkpin("1234"));
console.log(checkpin("12345"));
console.log(checkpin("123456"));
console.log(checkpin("1234567"));