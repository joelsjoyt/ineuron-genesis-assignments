let employee = {
  eId: [ 67, 48, 29],
  eName: [ "Hitanshu", "Ninad", "Amandeep"],
  eSlaary: [ 75000, 82000, 98000],
}


const showId = () =>{
  
  console.log(employee.eId);
}

const count = () =>{

  console.log(`Employee count ${employee.eId.length}`);
}

const showEmployee = () =>{
  for(let i =0; i< employee.eId.length; i++){
    console.log(`${employee.eId[i]} : ${employee.eName[i]} `)
  }
}

const showSalary = () =>{

  console.log(employee.eSlaary)
}

const calculateAvgSalary = () =>{
  let sum = 0;
  sum = employee.eSlaary.reduce((acc,curr) => {
   acc += curr;
   return acc;
  }, 0 );

  console.log(`Average Salary : ${sum / employee.eSlaary.length}`);
}

showId();
count();
showEmployee();
showSalary();
calculateAvgSalary();