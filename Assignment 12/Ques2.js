const student = new Map();

//Ques 1
student.set('id',[1, 2 ,3]);
student.set('name', [ 'Hitanshu', 'Ninad', 'Amandeep'])
student.set('scores', [ 90, 88, 92 ])

//Ques 4
console.log("-----one parameter-----");
console.log(student.values());
console.log("-----two parameter-----");
console.log(student.entries());
console.log("-----three parameter-----");
console.log(student);


//Ques 2
console.log('Names', student.get('name'));
//Ques 3
student.delete('scores');
console.log('Map view', student.entries());