/*
You can nest async functions in js
*/

const async1 = async () =>{
  await console.log('Async 1');
  const async2 = async () =>{
    await console.log('Async 2')
  }
  async2();
}

async1();