/*
You can use async await with promises
*/

let promiseCaller = async () =>{
  return await new Promise(resolve =>{
    resolve('Hello from promise')
  })
}

promiseCaller().then((x) => console.log(x))