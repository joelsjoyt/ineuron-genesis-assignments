function checkInput(value){
  if(value.toLowerCase() === 'yes'){
    console.log("Correct");
  }else{
    console.log("Incorrect");
  }
}

checkInput("Yes");
checkInput("yes");
checkInput("YES");
checkInput("yEs");
checkInput("No");