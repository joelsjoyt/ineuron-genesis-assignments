// // // Convert string to uppercase
var str = "hello";
console.log(str.toUpperCase());

// // //Convert to lowercase
var str1 = "Yo WAZZUP";
console.log(str1.toLowerCase());

// //First character to uppercase
val = "str";
console.log(val.charAt(0).toUpperCase() + val.slice(1)); 


// // Break String to Half and swap them
function breakStringSwap(value){
  let i = value.length / 2;
  temp1 = value.slice(0,i);
  temp2 = value.slice(i, value.length);
  temp2  = temp2.concat(" ",temp1);
  
  return temp2;

}

console.log(breakStringSwap("Hellomate")); 

// //Count repeating characters
function countRepeats(value){
  counts = new Object();
  for(let i = 0; i<value.length; i++){
    let count = 0;
    for(let j=0; j<value.length; j++){
      if(value[i].toLowerCase() === value[j].toLowerCase()){
        count += 1;
        counts[value[i]] = count
      }
    }
  }
  return counts;
}

str3 = "HeHkak"
console.log(countRepeats(str3));

// // String reversal
function stringReversal(value){
  let newValue = '';
  for( let i = value.length - 1; i>=0; i--){
    newValue += value[i];
  }

  return newValue;
}

console.log(stringReversal("Helo"));
