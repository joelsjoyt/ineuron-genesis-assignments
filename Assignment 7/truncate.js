function truncate(value){
  let temp = '';
  if(value.length > 4){
    temp = value.slice(0,3) + '...';
  }else{
    temp = value;
  }

  return temp;
}

console.log(truncate("Ice"));
console.log(truncate("Icecream"));

