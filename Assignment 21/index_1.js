//Question 1
document.getElementById('change').innerText = "Hello World"; 

//Question 2
const element = document.getElementById('change');
element.style.color = 'red';
element.style.fontWeight = 'bold';
element.style.textTransform = 'uppercase';

//Question 3
const add = () =>{
  let x = Number(document.getElementById('input_1').value);
  let y = Number(document.getElementById('input_2').value);
  console.log(x+y);
  document.getElementById('result').innerText = x + y;
}

//Question 5
const toggleButton = () =>{
  console.log('hello');

}

//Question 6
window.onbeforeunload = function(){
  return 'Leave page?'
}

//Question 8

const newElem = document.createElement("h4")
newElem.innerText = "H4 created"
document.body.appendChild(newElem)




//Question 9
const removeElem = () =>{
  let elem = document.getElementById('remove')
  elem.remove()
}


//Question 10
console.log('Total P tags',document.getElementsByTagName('p').length);
