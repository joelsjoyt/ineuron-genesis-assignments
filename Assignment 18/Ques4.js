function* multiplegenrator(x){
  yield x*x
}

let itr = multiplegenrator(2);
console.log(itr.next().value);
let it2 = multiplegenrator(3);
console.log(it2.next().value);
