function* errorGenerator(){
  throw new Error('Just an error')
}

try {
 let x = errorGenerator()
 x.next()
} catch (error) {
  console.log(error.message);
}