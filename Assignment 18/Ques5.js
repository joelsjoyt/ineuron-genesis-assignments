function* infiniteGenerator(){
  let x = 0;
  while(true){
    yield x + 1;
  }
}

let itr = infiniteGenerator()
res = itr.next()
while(!res.done){
  console.log(itr.next());
  res = itr.next()
}
