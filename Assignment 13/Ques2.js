let students = [
  { name: "John", marks: "92" },
  { name: "Oliver", marks: "85" },
  { name: "Michael", marks: "79" },
  { name: "Dwight", marks: "95"},
  { name: "Oscar", marks: "64" },
  { name: "Kevin", marks: "48" },
  ];

  
  let gradeA = [];
  let gradeB = [];
  let gradeC = [];
  let gradeD = [];
  let gradeE = [];
  let gradeF = [];

  let calculateGrades = (val) =>{
    switch (true) {
      case val.marks > 90 :
        val['Grade'] = 'A'
        gradeA.push(val)  
        break;
      case val.marks > 80 :
        val['Grade'] = 'B'
        gradeB.push(val) 
        break;
      case val.marks > 70 :
        val['Grade'] = 'C'
        gradeC.push(val) 
        break;
      case val.marks > 60 :
        val['Grade'] = 'D'
        gradeD.push(val) 
        break;
      case val.marks > 50 :
        val['Grade'] = 'E'
        gradeE.push(val) 
        break;
      case val.marks < 50 :
        val['Grade'] = 'F'
        gradeF.push(val) 
        break;    
      default:
        console.log("Canot calculate")
        break;
    }
  }

  //Calculating grade and grouping them
  students.map((student) => calculateGrades(student))
  //Display grades of students
  console.log(students)
  //Students grouped according to grades
  console.log("Grade A : ", gradeA)
  console.log("Grade B : ", gradeB)
  console.log("Grade C : ", gradeC)
  console.log("Grade D : ", gradeD)
  console.log("Grade E : ", gradeE)
  console.log("Grade F : ", gradeF)

  
  
